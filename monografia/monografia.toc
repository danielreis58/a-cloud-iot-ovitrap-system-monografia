\changetocdepth {4}
\babel@toc {brazil}{}
\babel@toc {brazil}{}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Introdu\IeC {\c c}\IeC {\~a}o}}{12}{chapter.1}
\contentsline {section}{\numberline {1.1}Motiva\IeC {\c c}\IeC {\~a}o}{12}{section.1.1}
\contentsline {section}{\numberline {1.2}Justificativa}{12}{section.1.2}
\contentsline {section}{\numberline {1.3}Objetivos}{13}{section.1.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Fundamenta\IeC {\c c}\IeC {\~a}o Te\IeC {\'o}rica}}{14}{chapter.2}
\contentsline {section}{\numberline {2.1}O que \IeC {\'e} uma ovitrap?}{14}{section.2.1}
\contentsline {section}{\numberline {2.2}Evolu\IeC {\c c}\IeC {\~a}o das ovitraps}{14}{section.2.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Desenvolvimento}}{17}{chapter.3}
\contentsline {section}{\numberline {3.1}A ideia inicial}{17}{section.3.1}
\contentsline {section}{\numberline {3.2}Ovitrap}{18}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Vaso maior}{20}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Vaso menor}{21}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Prato}{21}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Boleira}{22}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}Tela mosquiteira}{22}{subsection.3.2.5}
\contentsline {subsection}{\numberline {3.2.6}Breu}{23}{subsection.3.2.6}
\contentsline {subsection}{\numberline {3.2.7}Palitos de churrasco e fitas hellerman}{23}{subsection.3.2.7}
\contentsline {section}{\numberline {3.3}Hardware}{23}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}NodeMCU}{25}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}VL53L0X (Time-of-Flight)}{26}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}Software}{26}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Banco de dados}{26}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}API}{27}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Aplica\IeC {\c c}\IeC {\~a}o Cliente}{27}{subsection.3.4.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Vis\IeC {\~a}o geral do sistema}}{31}{chapter.4}
\contentsline {section}{\numberline {4.1}Alimenta\IeC {\c c}\IeC {\~a}o}{31}{section.4.1}
\contentsline {section}{\numberline {4.2}Comunica\IeC {\c c}\IeC {\~a}o}{32}{section.4.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {Custos}}{33}{chapter.5}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {6}\MakeTextUppercase {Valida\IeC {\c c}\IeC {\~a}o e resultados}}{34}{chapter.6}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {7}\MakeTextUppercase {Conclus\IeC {\~a}o e trabalhos futuros}}{35}{chapter.7}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{REFER\IeC {\^E}NCIAS}{36}{chapter*.44}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{APENDICE A - DOCUMENTO DE REQUISITOS}{38}{chapter*.45}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\vspace {\cftbeforechapterskip }
